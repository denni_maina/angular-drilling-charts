import { Component } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { NgModule, OnInit, ViewChild, ElementRef, VERSION } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import * as  Highcharts from 'highcharts';
import  More from 'highcharts/highcharts-more';
More(Highcharts);
import Drilldown from 'highcharts/modules/drilldown';
Drilldown(Highcharts);
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
import {NgZone} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@Component({
  selector: 'app',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  chartInstance: any = {};

  // Callback to get chart instance
  initialized(e) {
      this.chartInstance = e.chart; // Save it for further use

      // Configure Drilldown attributes 
      // See this : https://www.fusioncharts.com/dev/api/fusioncharts/fusioncharts-methods#configureLink
      this.chartInstance.configureLink({
          type: "pie3d",
          overlayButton: {
              message: 'close',
              fontColor: '880000',
              bgColor: 'FFEEEE',
              borderColor: '660000'
          }
      }, 0)
  }
  dataSource = {
      "chart": {
          "caption": "Top 3 Juice Flavors",
          "subcaption": "Last year",
          "xaxisname": "Flavor",
          "yaxisname": "Amount (In USD)",
          "numberprefix": "$",
          "theme": "fusion",
          "rotateValues": "0"
      },
      "data": [{
              "label": "jan",
              "value": "810000",
              "link": "newchart-xml-apple"
          },
          {
              "label": "feb",
              "value": "620000",
              "link": "newchart-xml-cranberry"
          },
          {
              "label": "mar",
              "value": "350000",
              "link": "newchart-xml-grapes"
          },
          {
            "label": "apr",
            "value": "350000",
            "link": "newchart-xml-grapes"
        },
        {
          "label": "may",
          "value": "350000",
          "link": "newchart-xml-grapes"
      },
      {
        "label": "jun",
        "value": "350000",
        "link": "newchart-xml-grapes"
    },
    {
      "label": "july",
      "value": "350000",
      "link": "newchart-xml-grapes"
  },
  {
    "label": "aug",
    "value": "350000",
    "link": "newchart-xml-grapes"
},
{
  "label": "sep",
  "value": "350000",
  "link": "newchart-xml-grapes"
},
{
  "label": "oct",
  "value": "350000",
  "link": "newchart-xml-grapes"
},
{
  "label": "nov",
  "value": "350000",
  "link": "newchart-xml-grapes"
},
{
  "label": "dec",
  "value": "350000",
  "link": "newchart-xml-grapes"
}
      ],
      "linkeddata": [{
              "id": "apple",
              "linkedchart": {
                  "chart": {
                      "caption": "Quarterly dispensation",
                      "subcaption": "Last year",
                      "numberprefix": "$",
                      "theme": "fusion",
                      "rotateValues": "0",
                      "plottooltext": "$label, $dataValue,  $percentValue"
                  },
                  "data": [{
                      "label": "week1",
                      "value": "157000"
                  }, {
                      "label": "week2",
                      "value": "172000"
                  }, {
                      "label": "week3",
                      "value": "206000"
                  }, {
                      "label": "week4",
                      "value": "275000"
                  }]
              }
          },
          {
              "id": "cranberry",
              "linkedchart": {
                  "chart": {
                      "caption": " Quarterly dispensation",
                      "subcaption": "Last year",
                      "numberprefix": "$",
                      "theme": "fusion",
                      "plottooltext": "$label, $dataValue,  $percentValue"
                  },
                  "data": [{
                          "label": "week1",
                          "value": "102000"
                      },
                      {
                          "label": "week2",
                          "value": "142000"
                      },
                      {
                          "label": "week3",
                          "value": "187000"
                      },
                      {
                          "label": "week4",
                          "value": "189000"
                      }
                  ]
              }
          },
          {
              "id": "grapes",
              "linkedchart": {
                  "chart": {
                      "caption": "Quarterly dispensation",
                      "subcaption": "Last year",
                      "numberprefix": "$",
                      "theme": "fusion",
                      "rotateValues": "0",
                      "plottooltext": "$label, $dataValue,  $percentValue"
                  },
                  "data": [{
                      "label": "week1",
                      "value": "45000"
                  }, {
                      "label": "week2",
                      "value": "72000"
                  }, {
                      "label": "week3",
                      "value": "95000"
                  }, {
                      "label": "week4",
                      "value": "108000"
                  }]
              }
          }
      ]
  };
  constructor(private zone: NgZone) {}
}